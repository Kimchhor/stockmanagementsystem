package main;
import model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;
public class Main {
    private static final String fileName = "product.txt";
    public static ArrayList<String> products = new ArrayList<>();
    private static Scanner cin = new Scanner(System.in);
    private static int numOfRows = 3;
    private static int currentPage = 1;
    private static Table table;
    public static final String sep = " ";

    public static void main(String[] args) throws InterruptedException {
        printTeamName();
        initData();
        getProducts(products);
        menu();

    }
    // kimchhor
    public static void printTeamName() {

        File file = new File("temp/BTBG2.txt");
        try {
            Scanner reader = new Scanner(file);
            System.out.println("\t\t\t\t\t\t\t\t\t\t" + "Welcome To Stock Management");
            while (reader.hasNextLine()) {
                String line = reader.nextLine();
                System.out.println("\t\t\t\t" + line);
                Thread.sleep(300);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private static void initData() {
        Thread t = new Thread(() -> {
            String init = "Please wait...";
            int i = 0;
            while (i < init.length()) {
                System.out.print(init.charAt(i++));
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        try {
            t.join();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            int flush = 0;


            for (int i = 1; i <= 9; i++) {
                Product product = new Product(i, "Coke", 4000, 100, getDate());
                writer.write(product.toString());
                writer.newLine();
                if (i == 5000 + flush) {
                    flush += 5000;
                    writer.flush();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static ArrayList<String> getProducts(ArrayList<String> products) {
        try (BufferedReader buffer = new BufferedReader(new FileReader("product.txt"))
        ) {
            long length = 0;
            String line;
            while ((line = buffer.readLine()) != null) {
                length++;
                products.add(line);
            }

            System.out.println("Current time loading: " + length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return products;
    }
    private static void menu() {
        do {
            String option = printMenu();
            switch (option.toLowerCase()) {
                case "*":
                    gotoPage(currentPage);
                    break;
                case "w":
                    writeData();
                    break;
                case "r":
                    readData();
                    break;
                case "u":
                    update();
                    break;
                case "d":
                    delete();
                    break;
                case "f":
                    goFirst();
                    break;
                case "p":
                    goPrevious();
                    break;
                case "n":
                    goNext();
                    break;
                case "l":
                    goLast();
                    break;
                case "s":
                    System.out.println("search by name");
                  //  search();
                    break;
                case "g":
                    gotoPage(Validator.readInt("Input page number(1-" + getTotalPage() + ") : ", 1, getTotalPage()));
                    break;
                case "se":
                    setRow();
                    break;
                case "ba":
                    //             backup();
                    break;
                case "sa":
                    //   saveOption("Do you want to save it? [Y/y] or [N/n] : ");
                    break;
                case "re":
                    //            reStore();
                    break;
                case "h":
                    //                help();
                    break;
                case "e":
                    System.exit(0);
                    break;
                case "a":
                    aboutus();
                    break;
                default:
                    System.out.println("Invalid Input");
                    break;
            }
        } while (true);
    }
    private static String printMenu() {
        String[] menu = {"*)Display", "W)rite", "R)ead",
                "U)pdate", "D)elete", "F)irst", "P)revious",
                "N)ext", "L)ast", "S)earch", "G)oto", "Se)t row",
                "Sa)ve", "Ba)ck up", "Re)store", "H)elp", "A)bout us", "E)xit"};
        printTable(9, 10, "Menu", menu, "tttttttttt");
        System.out.print("Command--> ");
        String str = cin.nextLine();
        return str;
    }
    //  Sengtit
    private static void update() {
        String product = updateById(Validator.readInt("Input ID : "), products, true);
        if (product != null)
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("temp\\Update.txt"))) {
                bufferedWriter.write(product);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            } catch (IOException e) {
            }
    }
    private static Product convertFromStringToProduct(String[] str) {
        return new Product(Integer.parseInt(str[0]), str[1], Double.parseDouble(str[2]), Integer.parseInt(str[3]), str[4]);
    }
    private static String insertRecord(Product p) {
        Product product = new Product();
        product.setId(p.getId());
        product.setName(p.getName());
        product.setQty(p.getQty());
        product.setUnitPrice(p.getUnitPrice());
        product.setImportedDate(p.getImportedDate());
        int orderNum;
        boolean loopStatus = true;

        do {
            String InsertMenu[] = {"1.Update All", "2.Name", "3.Unit Price", "4.Quantity", "5.Exit"};
            printTable(5, 10,"", InsertMenu, "tttttttttt");

            orderNum = Validator.readInt("Choose Option :");
            switch (orderNum) {
                case 1:
                    product = insertNewRecord();
                    product.setId(p.getId());
                    product.setImportedDate(p.getImportedDate());

                    break;
                case 2:
                    System.out.print("Name:");
                    String name = cin.nextLine();
                    product.setName(name);

                    break;
                case 3:
                    product.setUnitPrice(Validator.readDouble("Unit Price :"));
                    break;
                case 4:
                    product.setQty(Validator.readInt("Quantity :"));
                    //  isUpdated = true;
                    break;
                case 5:
                    loopStatus = false;
                    break;
                default:
                    System.out.println("Invalid Input");
                    break;
            }

        } while (loopStatus);


        while (true) {
            System.out.println("press 'y' to update and 'n' to cancel");

            char c = new Scanner(System.in).next().charAt(0);
            if (c == 'y' || c == 'Y') {
                return product.toString();
            } else if (c == 'n' || c == 'N') {
                return p.toString();
            }
        }
    }
    private static Product insertNewRecord() {
        System.out.print("Name:");
        String name = cin.nextLine();
        double price = Validator.readDouble("Unit Price : ");
        int qty = Validator.readInt("Quantity :");
        Product newRecord=new Product(0, name, price, qty, null);
        return newRecord;
    }
    public static String updateById(int number, ArrayList<String> products, boolean b) {
        int index = findProductToUpdate(number, products);
        String productString = null;
        if (index == -1) {
            return null;
        } else {
            if (b) {//update record
                Product product = convertFromStringToProduct(subString(products.get(index)));
                productString = insertRecord(product);
                products.set(index, productString);
            } else {//delete record
                if (Validator.readYesNo("press 'y' to delete and 'n' to cancel : ") == 'y') {
                    productString = products.remove(index);
                    String str[] = subString(productString);
                    String myString[] ={"ID", str[0], "Name", str[1], "Unit Price", str[2], "Quantity", str[3], "Imported Date", str[4]};
                    printTable(2,20,"",myString,"tttttttttt");

                    System.out.println("Deleted successfully");

                }else{
                    System.out.println("Delete canceled");
                }
            }

            return productString;
        }
    }
    private static int findProductToUpdate(Integer id, ArrayList<String> products) {

        for (int i = 0; i < products.size(); i++) {
            String[] str = subString(products.get(i));
            if (Integer.parseInt(str[0]) == id) {
                String myString[] ={"ID", str[0], "Name", str[1], "Unit Price", str[2], "Quantity", str[3], "Imported Date", str[4]};
                printTable(2,20,"",myString,"tttttttttt");
                return i;
            }
        }

        System.out.println("Not Found");
        return -1;
    }
    public static String[] subString(String str) {

        int firstIndex = str.indexOf(sep);
        String s1 = str.substring(0, firstIndex);

        int second = str.indexOf(sep, firstIndex + 1);
        String s2 = str.substring(firstIndex + 1, second);

        int third = str.indexOf(sep, second + 1);
        String s3 = str.substring(second + 1, third);

        int fourth = str.indexOf(sep, third + 1);
        String s4 = str.substring(third + 1, fourth);

        String s5 = str.substring(fourth + 1);

        return new String[]{s1, s2, s3, s4, s5};
    }
    private static void delete() {

        String product = updateById(Validator.readInt("Input ID : "), products, false);
        if (product != null)
            if (currentPage > getTotalPage())
                currentPage = 1;
    }
    // panha
    private static void readData() {
        int id = Validator.readInt("Read by ID :");
        readData(id);
    }
    private static void readData(int id) {
        for (String product : products) {
            String[] split = product.split(sep);
            if (id == Integer.parseInt(split[0])) {
                String shown[] = {"ID", split[0], "Name", split[1], "Unit Price", split[2], "Quantity", split[3], "Imported Date", split[4]};
                printTable(2, 20, "", shown, "tttttttttt");
            }
        }
    }
    private static void writeData() {
        String[] lastProduct = products.get(products.size() - 1).split(sep);
        int lastId = Integer.parseInt(lastProduct[0]);
        System.out.println("Product ID : " + (lastId + 1));

        System.out.print("Product's Name : ");
        String name = cin.nextLine();
        if (Pattern.matches(".*" + "\\|" + ".*", name)) {
            System.out.println("Invalid!");
            writeData();
            return;
        }

        double price = Validator.readDouble("Unit Price : ");
        int qty = Validator.readInt("Quantity : ", 1, 10000000);

        char answer;

        System.out.print("Are you sure to add record? [Y/y] or [N/n]:");
        answer = Character.toLowerCase(cin.next().charAt(0));
        if (answer == 'y') {
            String product = ("" + (lastId + 1) + sep + name + sep + price + sep + qty + sep + getDate());
            products.add(product);
            try {
                BufferedWriter insertFile = new BufferedWriter(new FileWriter("temp\\Insert.txt", true));
                insertFile.write(product);
                insertFile.newLine();
                insertFile.flush();
                insertFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cin.nextLine();
    }
    private static void gotoPage(int pageNum) {
        tableHeader();
        currentPage = pageNum;
        int start = numOfRows * (currentPage - 1);

        if (pageNum > getTotalPage()) return;
        if (pageNum == getTotalPage()) {
            goLast();
        } else {

            for (int i = start; i < start + numOfRows; i++) {
                addRowTable(products.get(i));
            }

        }
        String[] myPageDetail = pagination();
        table.addCell(myPageDetail[0], new CellStyle(CellStyle.HorizontalAlign.left), 2);
        table.addCell(myPageDetail[1], new CellStyle(CellStyle.HorizontalAlign.right), 3);
        System.out.println(table.render());
        pagination();
    }
    //Pich
    private static void setRow() {
        System.out.print("Number of row : ");
        numOfRows = cin.nextInt();
        if (currentPage > getTotalPage())
            currentPage = 1;
        cin.nextLine();
    }
    private static void goNext() {
        if (currentPage != getTotalPage())
            gotoPage(++currentPage);
        else gotoPage(getTotalPage());
    }
    private static void goPrevious() {
        if (currentPage != 1)
            gotoPage(--currentPage);
        else
            gotoPage(1);
    }
    private static void goFirst() {
        currentPage = 1;
        tableHeader();
        for (int i = 0; i < numOfRows; i++) {
            addRowTable(products.get(i));
        }
        String[] myPageDetail = pagination();
        table.addCell(myPageDetail[0], new CellStyle(CellStyle.HorizontalAlign.left), 2);
        table.addCell(myPageDetail[1], new CellStyle(CellStyle.HorizontalAlign.right), 3);
        System.out.println(table.render());
    }
    private static void goLast() {
        tableHeader();
        currentPage = getTotalPage();
        int start = numOfRows * (currentPage - 1);
        for (int i = start; i < products.size(); i++) {
            addRowTable(products.get(i));
        }
        String[] myPageDetail = pagination();
        table.addCell(myPageDetail[0], new CellStyle(CellStyle.HorizontalAlign.left), 2);
        table.addCell(myPageDetail[1], new CellStyle(CellStyle.HorizontalAlign.right), 3);
        System.out.println(table.render());
    }
    // piseth
    private static int getTotalPage() {
        return products.size() % numOfRows == 0 ? products.size() / numOfRows : products.size() / numOfRows + 1;
    }
    private static String[] pagination() {
        return new String[]{"Page : " + currentPage + " of " + getTotalPage(), "Total Record: " + products.size()};
    }
    public static void addRowTable(String product) {
        String[] p = product.split(sep);
        for (int i = 0; i < 5; i++)
            table.addCell(p[i]);
    }
    private static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();

        return dateFormat.format(date);
    }
    private static void tableHeader() {
        BorderStyle borderStyle = new BorderStyle("╔═", "═", "═╤═", "═╗", "╟─", "─", "─┼─", "─╢", "╚═", "═", "═╧═", "═╝", "║ ", " │ ", " ║", "─┴─", "─┬─");
        table = new Table(5, borderStyle, new ShownBorders("tttttttttt"));
        int myMinWidth[] = {14, 32, 12, 11, 18};
        for (int i = 0; i < 5; i++) {
            table.setColumnWidth(i, myMinWidth[i], 27);
        }
        table.addCell("ID", new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("Name", new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("Unit Price", new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell("Quantity", new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell("Date", new CellStyle(CellStyle.HorizontalAlign.left));
    }
    public static void printTable(int colNum, int colWidth, String content, String[] Value, String shown) {
        BorderStyle borderStyle = new BorderStyle("╔═", "═", "═╤═", "═╗", "╟─", "─", "─┼─", "─╢", "╚═", "═", "═╧═", "═╝", "║ ", " │ ", " ║", "─┴─", "─┬─");
        Table tbl = new Table(colNum, borderStyle, new ShownBorders(shown));
        if (content != "")
            tbl.addCell(content, new CellStyle(CellStyle.HorizontalAlign.center), colNum);
        for (int i = 0; i < colNum; i++) {
            tbl.setColumnWidth(i, colWidth, colWidth + 10);
        }
        for (int i = 0; i < Value.length; i++) {
            tbl.addCell(Value[i]);
        }
        System.out.println(tbl.render());
    }
    private static void aboutus() {
        System.out.println("Team members: ");
        System.out.println("1.  Theng Sovannpich");
        System.out.println("2.  Chiv Kimchhor");
        System.out.println("3.  Thou Piseth");
        System.out.println("4.  Tray Sengtit");
        System.out.println("5.  Youen Sopanha");
    }


}
