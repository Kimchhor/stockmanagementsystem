package main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Validator {
    private static Scanner input = new Scanner(System.in);
    public static int readInt(String message){
        int value = 0;
        boolean validValue = false;
        while (!validValue){
            try {
                System.out.print(message);
                value = input.nextInt();
                validValue = true;
            }catch (InputMismatchException e){
                System.out.println("Invalid input!");
                input.nextLine();
            }
        }
        return value;
    }

    public static int readInt(String message, int min, int max){
        int value = readInt(message);
        while (value < min || value > max){
            System.out.println("Input is negative");
            value = readInt(message);
        }
        return value;
    }
    public static double readDouble(String message, int min, int max){
        double value = readDouble(message);
        while (value < min || value > max){
            System.out.println("Input is negative");
            value = readDouble(message);
        }
        return value;
    }
    public static double readDouble(String message){
        double value = 0;
        boolean validValue = false;
        while (!validValue){
            try {
                System.out.print(message);
                value = input.nextDouble();
                validValue = true;
            }catch (InputMismatchException e){
                System.out.println("Invalid input!");
                input.nextLine();
            }
        }
        return value;
    }
    public static char readYesNo(String message){
        char value = ' ';
        boolean validValue = false;
        while (!validValue){
            System.out.print(message);
            value = input.next().toLowerCase().charAt(0);
            if (value == 'y' || value == 'n')
                validValue = true;
        }
        return value;
    }

}
