Our available features of the project included:
1. Display : to display all products
2. Write : add products into to file
3. read :read products from the file
4. update: we can update
    4.1: all
    4.2: Name
    4.3: Unit Price
    4.4: Quantity
5. delete
6. since our products are stored in a table that has many page so there are some feature related to page such as
    6.1 go to: for go to any page 
    6.2 first: for go to the first page
    6.3 previous : for go to the previous page
    6.4 next : for go to the next page
    6.5 last : for go to the last page 
7. set row: for modify number of row of the table
8. Exit
